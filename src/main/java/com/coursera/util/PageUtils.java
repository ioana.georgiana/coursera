package com.coursera.util;

import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class PageUtils {
    private static final Logger LOG = Logger.getLogger(PageUtils.class.getName());

    public static boolean isElementPresent(WebElement element) {
        boolean present = false;
        try {
            present = element.isDisplayed();
        } catch (NoSuchElementException e) {
            LOG.info("Element was not found." + e);
        }
        return present;
    }

    public static WebElement waitForElement(WebElement element) {
        ExpectedCondition<WebElement> condition = ExpectedConditions.visibilityOf(element);
        return WebDriverUtil.getInstance().getWebDriverWait().until(condition);
    }

    public static WebElement waitForElementBy(By by) {
        return WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public static List<WebElement> waitForNestedElements(WebElement parent, By by) {
        ExpectedCondition<List<WebElement>> conditions = ExpectedConditions.visibilityOfNestedElementsLocatedBy(parent, by);
        return WebDriverUtil.getInstance().getWebDriverWait().until(conditions);
    }

    public static void waitForPageToBeLoaded() {
        WebDriverUtil.getInstance().getWebDriverWait().until(input -> executeJS("return document.readyState").equals("complete"));
    }

    private static Object executeJS(String script, Object... args) {
        JavascriptExecutor executor = (JavascriptExecutor) WebDriverUtil.getInstance().getWebDriver();
        return executor.executeScript(script, args);
    }

    public static void waitForElementNotToBePresent(WebElement element) {
        WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.invisibilityOf(element));
    }

    public static void waitForPresenceOfElement(WebElement element) {
        WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForPresenceOfElement(By by) {
        WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void waitForElementToBeClickable(WebElement element) {
        WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.elementToBeClickable(element));
    }

    public static WebElement waitForElementToBeVisible(By by) {
        return WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void waitForElementToBeVisible(WebElement webElement) {
        WebDriverUtil.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOf(webElement));
    }

    public static boolean waitForPresenceOfText(WebElement webElement, String text) {
        return WebDriverUtil.getInstance().getWebDriverWait().withTimeout(5, TimeUnit.SECONDS).until(
                ExpectedConditions.textToBePresentInElement(webElement, text));
    }

    public static void waitForVisibilityOfAll(List<WebElement> elements) {
        WebDriverUtil.getInstance().getWebDriverWait().withTimeout(5, TimeUnit.SECONDS).until(
                ExpectedConditions.visibilityOfAllElements(elements));
    }
}
