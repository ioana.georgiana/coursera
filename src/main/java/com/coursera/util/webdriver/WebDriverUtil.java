package com.coursera.util.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverUtil {
    private static WebDriverUtil instance;
    private final Browser browser = Browser.valueOf(System.getProperty("browser").toUpperCase());
    private WebDriver webdriver;
    private WebDriverWait webDriverWait;

    public static WebDriverUtil getInstance() {
        if (instance == null) {
            instance = new WebDriverUtil();
        }

        return instance;
    }

    public void quit() {
        if (webdriver != null) {
            webdriver.manage().deleteAllCookies();
            webdriver.quit();
            webdriver = null;
        }
    }

    public void recreateIfClosed() {
        if (webdriver == null) {
            createWebDriver();
        }
    }

    public WebDriver getWebDriver() {
        return webdriver;
    }

    public WebDriverWait getWebDriverWait() {
        return webDriverWait;
    }

    private void createWebDriver() {
        webdriver = browser.getDriver();
        webdriver.manage().window().maximize();
        webDriverWait = new WebDriverWait(webdriver, 10);
    }
}
