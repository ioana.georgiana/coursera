package com.coursera.util.webdriver;

import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;

public class WebDriverHelper {
    static void setChromeWebDriverProperty() {
        System.setProperty("webdriver.chrome.driver", getWebDriverPath("chromedriver"));
    }

    static void setFirefoxWebDriverProperty() {
        System.setProperty("webdriver.gecko.driver", getWebDriverPath("geckodriver"));
    }

    static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        options.setExperimentalOption("prefs", prefs);

        return options;
    }

    private static String getWebDriverPath(String webDriver) {
        String osName = System.getProperty("os.name");
        String driverPath = System.getProperty("user.dir") + "/driver/";

        if (osName.contains("Windows")) {
            return driverPath + webDriver + ".exe";
        }

        if (osName.contains("nux") || osName.contains("nix") || osName.contains("aix")) {
            return driverPath + webDriver + "-linux";
        }

        return driverPath + webDriver;
    }
}
