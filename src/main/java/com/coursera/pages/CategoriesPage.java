package com.coursera.pages;

import com.coursera.util.PageUtils;
import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CategoriesPage {
    @FindBy(css = ".rc-DomainNav .categoryTitle")
    private WebElement categoriesTitle;

    @FindBy(css = ".rc-DomainNav .rc-DomainNavItem .domain-name")
    private List<WebElement> categoriesList;

    public CategoriesPage() {
        PageFactory.initElements(WebDriverUtil.getInstance().getWebDriver(), this);
    }

    public WebElement getCategoriesTitle() {
        return categoriesTitle;
    }

    public boolean isCategoryInTheList(String category) {
        PageUtils.waitForVisibilityOfAll(categoriesList);
        return categoriesList.stream()
                .anyMatch(webElement -> webElement.getText().trim().equals(category));
    }
}
