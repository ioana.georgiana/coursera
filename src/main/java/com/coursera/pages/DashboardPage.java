package com.coursera.pages;

import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {

    @FindBy(css = ".rc-PageList .c-page-item div[data-track-component='left_nav_my_courses']")
    private WebElement myCoursesButton;

    @FindBy(css = ".rc-PageList .c-page-item div[data-track-component='left_nav_accomplishments']")
    private WebElement recommendationsButton;

    @FindBy(css = "div[data-track-page='recommendations'] div>h1")
    private WebElement optionTitle;

    public DashboardPage() {
        PageFactory.initElements(WebDriverUtil.getInstance().getWebDriver(), this);
    }

    public WebElement getMyCoursesButton() {
        return myCoursesButton;
    }

    public WebElement getRecommendationsButton() {
        return recommendationsButton;
    }

    public WebElement getOptionTitle() {
        return optionTitle;
    }
}
