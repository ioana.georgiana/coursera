package com.coursera.pages;

import com.coursera.util.PageUtils;
import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.NoSuchElementException;

public class MainHeader {

    @FindBy(className = "c-logo")
    private WebElement courseraLogoImage;

    @FindBy(className = "c-catalog-button")
    private WebElement catalogButton;

    @FindBy(css = "form input[placeholder='Search catalog']")
    private WebElement searchCatalogInput;

    @FindBy(css = ".c-search-icon-wrapper")
    private WebElement searchCatalogButton;

    @FindBy(className = "c-ph-enterprise")
    private WebElement forEnterpriseLink;

    @FindBy(className = "c-ph-log-in")
    private WebElement logInLink;

    @FindBy(className = "c-ph-sign-up")
    private WebElement signUpButton;

    @FindBy(className = "c-ph-username")
    private WebElement logedInUserName;

    @FindBy(css = ".c-authenticated-dropdown-menu-container button")
    private WebElement loggedUserDropdownMenu;

    @FindBy(css = "#c-ph-aai-dropdown li")
    private List<WebElement> dropDownMenuList;

    public MainHeader() {
        PageFactory.initElements(WebDriverUtil.getInstance().getWebDriver(), this);
    }

    public WebElement getCourseraLogoImage() {
        return courseraLogoImage;
    }

    public WebElement getLogInLink() {
        return logInLink;
    }

    public void clickOnLogin() {
        PageUtils.waitForElementToBeClickable(logInLink);
        logInLink.click();
        PageUtils.waitForElementToBeVisible(By.className("c-user-modal-content"));
    }

    public void searchInCatalog(String course) {
        searchCatalogInput.clear();
        searchCatalogInput.sendKeys(course);
        searchCatalogButton.click();
    }

    public void clickOnSignUp() {
        signUpButton.click();
        PageUtils.waitForElementToBeVisible(By.className("c-user-modal-content"));
    }

    public void goToHomePage() {
        courseraLogoImage.click();
    }

    public void clickOnCatalogButton() {
        PageUtils.waitForElementToBeClickable(catalogButton);
        catalogButton.click();
    }

    public WebElement getLogedInUserName() {
        return logedInUserName;
    }

    public void clickOnLoggedUserDropdownMenu() {
        loggedUserDropdownMenu.click();
    }

    public void clickOnDropdownMenuOption(String option) {
        dropDownMenuList.stream()
                .filter(webElement -> option.equals(webElement.getText().trim()))
                .findFirst()
                .orElseThrow(NoSuchElementException::new)
                .click();
    }
}
