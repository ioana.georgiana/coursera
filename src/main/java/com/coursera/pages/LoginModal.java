package com.coursera.pages;

import com.coursera.util.PageUtils;
import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginModal {
    @FindBy(css = ".c-user-modal-controls input[name='email']")
    private WebElement loginEmailInput;

    @FindBy(css = ".c-user-modal-controls input[name='password']")
    private WebElement loginPasswordInput;

    @FindBy(css = ".rc-LoginForm button[data-js='submit']")
    private WebElement loginButton;

    @FindBy(css = ".rc-LoginForm .c-user-modal-facebook-button")
    private WebElement logInWithFacebookButton;

    public LoginModal() {
        PageFactory.initElements(WebDriverUtil.getInstance().getWebDriver(), this);
    }

    public void logIn(String email, String password) {
        PageUtils.waitForPresenceOfElement(loginEmailInput);
        loginEmailInput.clear();
        loginEmailInput.sendKeys(email);
        loginPasswordInput.clear();
        loginPasswordInput.sendKeys(password);
        loginButton.click();
    }
}
