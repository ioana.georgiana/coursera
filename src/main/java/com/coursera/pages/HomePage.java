package com.coursera.pages;

import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    @FindBy(css = ".content-container button")
    private WebElement joinForFreeButton;

    public HomePage() {
        PageFactory.initElements(WebDriverUtil.getInstance().getWebDriver(), this);
    }

    public WebElement getJoinForFreeButton() {
        return joinForFreeButton;
    }
}
