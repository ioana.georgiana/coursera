package com.coursera.pages;

import com.coursera.util.webdriver.WebDriverUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchedCoursesPage {
    @FindBy(css = ".c-you-searched-for>span")
    private WebElement searchConfirmationText;

    public SearchedCoursesPage() {
        PageFactory.initElements(WebDriverUtil.getInstance().getWebDriver(), this);
    }

    public WebElement getSearchConfirmationText() {
        return searchConfirmationText;
    }
}
