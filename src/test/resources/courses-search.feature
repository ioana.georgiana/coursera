@smoke
Feature: As a Coursera user, I can search for specific courses

  Scenario: As a non-loged-in user, I can see the catalog and search for courses
    Given I open the application
    Then I see the home page
    When I search for "Java" in the search engine from the header
    Then I should see the results and the text "You searched for Java" displayed
    When I click on the Catalog button
    Then I see the "Business" category in the list
    And I see the "Math and Logic" category in the list
    And I see the "Arts and Humanities" category in the list
    And I see the "Data Science" category in the list
    And I see the "Computer Science" category in the list


  Scenario: As a loged-in user, I can see the catalog and search for courses
    Given I open the application
    Then I see the home page
    And I log in  with "ioana.iacob.geo@gmail.com"
    Then I can see on the dashboard my courses button
    When I search for "PHP" in the search engine from the header
    Then I should see the results and the text "You searched for PHP" displayed
    When I click on the Catalog button
    Then I see the "Business" category in the list
    And I see the "Math and Logic" category in the list
