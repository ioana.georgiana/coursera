@smoke
Feature: As a logged in user, I can log out

  Scenario: Log into the application and then log out
    Given I open the application
    When I log in  with "ioana.iacob.geo@gmail.com"
    Then I am redirected to my "Ioana" user dashboard
    When I click on the dropdown menu from the right of the main header
    And I choose option "Log Out"
    Then I see the home page
    And I see the Log in option present in the header

