@smoke
Feature: As a Coursera user, I want to be able to login

  Scenario: I open the application and I login
    Given I open the application
    Then I see the home page
    And I see the Coursera logo in the main header
    When I log in  with "ioana.iacob.geo@gmail.com"
    Then I can see on the dashboard my courses button
    When I click on Recommendations button
    Then I see "Your Recommendations" title displayed
