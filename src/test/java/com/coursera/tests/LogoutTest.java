package com.coursera.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "com/coursera/steps",
        features = "src/test/resources/logout.feature",
        plugin = {"json:target/results/logout.json"}
)
public class LogoutTest {
}
