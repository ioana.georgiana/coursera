package com.coursera.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "com/coursera/steps",
        features = "src/test/resources/courses-search.feature",
        plugin = {"json:target/results/courses-search.json"}
)
public class CoursesSearchTest {
}
