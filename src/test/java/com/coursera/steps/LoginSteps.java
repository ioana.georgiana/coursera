package com.coursera.steps;

import com.coursera.pages.LoginModal;
import com.coursera.pages.MainHeader;
import com.coursera.util.EnvironmentConfiguration;
import cucumber.api.java.en.When;

public class LoginSteps {
    private MainHeader mainHeader = new MainHeader();
    private LoginModal loginModal = new LoginModal();

    @When("^I log in  with \"([^\"]*)\"$")
    public void iLogInWith(String userMail) {
        mainHeader.clickOnLogin();
        loginModal.logIn(userMail, EnvironmentConfiguration.getInstance().get(userMail));
    }
}
