package com.coursera.steps;

import com.coursera.pages.SearchedCoursesPage;
import com.coursera.util.PageUtils;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class SearchedCoursesSteps {
    private SearchedCoursesPage searchedCoursesPage = new SearchedCoursesPage();

    @Then("^I should see the results and the text \"([^\"]*)\" displayed$")
    public void iShouldSeeTheResultsAndTheTextDisplayed(String text) {
        Assert.assertTrue(PageUtils.waitForPresenceOfText(searchedCoursesPage.getSearchConfirmationText(), text));
    }
}
