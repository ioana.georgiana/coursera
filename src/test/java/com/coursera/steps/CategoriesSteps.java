package com.coursera.steps;

import com.coursera.pages.CategoriesPage;
import com.coursera.util.PageUtils;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class CategoriesSteps {
    private CategoriesPage categoriesPage = new CategoriesPage();

    @Then("^I see the \"([^\"]*)\" label displayed above the list$")
    public void iSeeTheLabelDisplayedAboveTheList(String label) {
        PageUtils.waitForPresenceOfElement(categoriesPage.getCategoriesTitle());
        Assert.assertEquals(label, categoriesPage.getCategoriesTitle().getText().trim());
    }

    @Then("^I see the \"([^\"]*)\" category in the list$")
    public void iSeeTheCategoryInTheList(String option) {
        Assert.assertTrue(categoriesPage.isCategoryInTheList(option));
    }
}
