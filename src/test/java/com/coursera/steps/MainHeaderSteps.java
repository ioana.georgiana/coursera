package com.coursera.steps;

import com.coursera.pages.MainHeader;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class MainHeaderSteps {
    private MainHeader mainHeader = new MainHeader();

    @And("^I click on the dropdown menu from the right of the main header$")
    public void iClickOnTheDropdownMenuFromTheRightOfTheMainHeader() {
        mainHeader.clickOnLoggedUserDropdownMenu();
    }

    @When("^I choose option \"([^\"]*)\"$")
    public void iChooseOption(String option) {
        mainHeader.clickOnDropdownMenuOption(option);
    }

    @When("^I click on the Catalog button$")
    public void iClickOnTheCatalogButton() {
        mainHeader.clickOnCatalogButton();
    }

    @When("^I search for \"([^\"]*)\" in the search engine from the header$")
    public void iSearchForInTheSearchEngineFromTheHeader(String searchOption) {
        mainHeader.searchInCatalog(searchOption);
    }

    @And("^I see the Log in option present in the header$")
    public void iSeeTheLogInOptionPresentInTheHeader() {
        Assert.assertTrue(mainHeader.getLogInLink().isDisplayed());
    }
}
