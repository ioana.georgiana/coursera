package com.coursera.steps;

import com.coursera.util.EnvironmentConfiguration;
import com.coursera.util.webdriver.WebDriverUtil;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;

public class BaseSteps {
    @Given("^I open the application$")
    public void openTheApplication() {
        WebDriverUtil.getInstance().recreateIfClosed();
        WebDriverUtil.getInstance().getWebDriver().navigate().to(EnvironmentConfiguration.getInstance().get("url"));
    }

    @After
    public void closeBrowser() {
        WebDriverUtil.getInstance().quit();
    }
}
