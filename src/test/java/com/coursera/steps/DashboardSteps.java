package com.coursera.steps;

import com.coursera.pages.DashboardPage;
import com.coursera.pages.MainHeader;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class DashboardSteps {
    private MainHeader mainHeader = new MainHeader();
    private DashboardPage dashboardPage = new DashboardPage();

    @Then("^I can see on the dashboard my courses button$")
    public void iCanSeeOnTheDashboardMyCoursesButton() {
        Assert.assertTrue(dashboardPage.getMyCoursesButton().isDisplayed());
    }

    @When("^I click on Recommendations button$")
    public void iClickOnRecommendationsButton() {
        dashboardPage.getRecommendationsButton().click();
    }

    @Then("^I see \"([^\"]*)\" title displayed$")
    public void iSeeTitleDisplayed(String optionTitle) {
        Assert.assertEquals(optionTitle, dashboardPage.getOptionTitle().getText().trim());
    }

    @Then("^I am redirected to my \"([^\"]*)\" user dashboard$")
    public void iAmRedirectedToMyUserDashboard(String userName) {
        Assert.assertEquals(userName, mainHeader.getLogedInUserName().getText().trim());
    }
}
