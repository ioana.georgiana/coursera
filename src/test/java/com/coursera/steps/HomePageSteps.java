package com.coursera.steps;

import com.coursera.pages.HomePage;
import com.coursera.pages.MainHeader;
import com.coursera.util.PageUtils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;

public class HomePageSteps {
    private HomePage homePage = new HomePage();
    private MainHeader mainHeader = new MainHeader();

    @Then("^I see the home page$")
    public void iSeeTheHomePage() {
        PageUtils.waitForPresenceOfElement(By.className("c-logo"));
        PageUtils.waitForElementToBeVisible(By.cssSelector(".content-container button"));
        Assert.assertEquals("Join for free", homePage.getJoinForFreeButton().getText().trim());
    }

    @And("^I see the Coursera logo in the main header$")
    public void iSeeTheCourseraLogoInTheMainHeader() {
        Assert.assertTrue(mainHeader.getCourseraLogoImage().isDisplayed());
    }
}
